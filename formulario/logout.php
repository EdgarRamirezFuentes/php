<?php
    session_start();
    // Elimina los datos de la sesión actual
    if (isset($_SESSION['num_cta'])) { unset($_SESSION["num_cta"]); }
    // Redirecciona a raíz
    header('Location: ./');
?>