<?php
    session_start();
    // Faltan demasiadas validaciones del lado del servidor que realizar, 
    // pero es funcional por el momento
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $usuarios = $_SESSION['usuarios'];
        $num_cta = (int) $_POST['num_cta'];
        if (!isset($usuarios[$num_cta])) {
            $datos = array(
                'num_cta' => $num_cta,
                'nombre' => $_POST['nombre'],
                'primer_apellido' => $_POST['primer_apellido'],
                'segundo_apellido' => $_POST['segundo_apellido'],
                'contrasenia' => $_POST['pass'],
                'genero' => $_POST['genero'],
                'fecha_nac' => date('d/m/Y', strtotime($_POST['fecha_nac'])),
            );
            $usuarios = &$_SESSION['usuarios'];
            $usuarios[$num_cta] = $datos;
            echo 'true';
        } else {
            echo 'false';
        }
    } else {
        // Solo se puede ingresar mediante petición POST
        header('Location: ./');
    }
?>