<?php 
    session_start();
    if(!isset($_SESSION['usuarios'])) {
        // Inicializa la "DB" de datos  de nuestros usuarios
        $_SESSION['usuarios'] = [
            1 => [
                'num_cta' => 1,
                'nombre' => 'Admin',
                'primer_apellido' => 'General',
                'segundo_apellido' => '',
                'contrasenia' => 'adminpass123.',
                'genero' => 'O',
                'fecha_nac' => '25/01/1990'
            ]
        ];
    }
?>