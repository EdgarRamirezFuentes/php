<?php 
    session_start();
    // Valida que se tenga una sesión abierta
    if (!isset($_SESSION['num_cta'])) {
        header('Location: ./');
    }
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formularios</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <header>
        <?php
            if (!isset($_SESSION['num_cta'])) {
                require_once('./navigation_bar_default.php');
            } else {
                require_once('./navigation_bar_login.php');
            }
        ?>
    </header>
    <main>
        <section>
            <article class="container">
                <h1 class="text-center">Bienvenido</h1>
                <p class="my-5 fs-3">Usuario autenticado</p>
                <?php
                    $datos = $_SESSION['num_cta'];
                    echo '
                    <div class="card">
                        <div class="card-header">'.
                            $datos['nombre'].' '.$datos['primer_apellido'].' '.$datos['segundo_apellido'].
                        '</div>
                        <div class="card-body">
                            <h5 class="card-title">Información</h5>
                            <p class="card-text my-3">Número de cuenta: '.$datos['num_cuenta'].'</p>
                            <p class="card-text my-3">Fecha nacimiento: '.$datos['fecha_nac'].'</p>
                        </div>
                    </div>
                    ';
                ?>
            </article>
            <article class="container">
                <p class="my-5 fs-3">Datos guardados</p>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha nacimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $usuarios_registrados = $_SESSION['usuarios'];
                        foreach ($usuarios_registrados as $usuario) {
                            echo '
                            <tr>
                                <th scope="row">'.$usuario['num_cta'].'</th>
                                <td>'.$usuario['nombre'].' '.$usuario['primer_apellido'].' '.$usuario['segundo_apellido'].'</td>
                                <td>'.$usuario['fecha_nac'].'</td>
                            </tr>
                            ';
                        }
                        ?>
                    </tbody>
                </table>
            </article>
        </section>
    </main>
    <script src="./bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

</body>

</html>