<?php 
    session_start();
    require_once('./init_data.php');
    // Si no existe una sesión, redirecciona al inicio de sesión
    if (!isset($_SESSION['num_cta'])) {
        require_once('./login.php');
    } else{ 
        // En caso de haber una sesión iniciada, redirecciona a la página de info
        require_once('./info.php');
    }
?>