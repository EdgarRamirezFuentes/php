<?php
    session_start();
    // Valida que no se tenga una sesión iniciada
    if (isset($_SESSION['num_cta'])) {
        header('Location: ./');
    }
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formularios</title>
    <link rel="stylesheet" href="./css/global.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <header>
        <?php
            if (!isset($_SESSION['num_cuenta'])) {
                require_once('./navigation_bar_default.php');
            } else {
                require_once('./navigation_bar_login.php');
            }
        ?>
    </header>
    <main>
        <section>
            <article id="form-container" class="d-flex justify-content-center align-items-center flex-column">
                <form id="login-form" action="./validate_login.php" method="post">
                    <div class="d-flex justify-content-center align-items-center flex-column">
                        <label for="num_cta" class="my-3">N&uacute;mero de cuenta</label>
                        <input class="form-control" type="text" name="num_cta" id="num_cta" >
                        <label for="pass" class="my-3">Contrase&ntilde;a</label>
                        <input class="form-control" type="password" name="pass" id="pass" >
                        <button id="btn-submit" class="btn btn-primary my-3">Iniciar sesi&oacute;n</button>
                    </div>
                </form>
            </article>
        </section>
    </main>
    <script src="./bower_components/jquery/dist/jquery.min.js"></script>
    <script src="./bower_components/jquery-validation/dist/jquery.validate.min.js"></script>r
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <!-- CUSTOM SCRIPTS -->
    <script src="./js/login.js"></script>

</body>

</html>