$(window).on('load', (e) => {

    $('#login-form').validate({
        rules: {
            num_cta : {
                required : true
            }, 
            pass : { 
                required : true
            }
        },
        messages : {
            num_cta : {
                required : "Número de cuenta requerida",
            },
            pass : {
                required : "Contraseña requerida"
            }
        },
        submitHandler : function (form) {
            $.ajax({
                url : form.action,
                type : form.method,
                data : $(form).serialize(),
                success : function (response) {
                    if (response === 'false') {
                        $('#wrong-credentials').remove();
                        const error_message = `<p id="wrong-credentials" class="error">Numero de cuenta/Contraseña incorrecto</p>`
                        $("#form-container").prepend(error_message);
                        console.log(response);
                    } else {
                        $('#wrong-credentials').remove();
                        $(window).attr("location","./");
                        console.log(response);
                    }
                    
                },
                error : function () {
					console.log('error');
				}

            });
        }
    });



});