$(window).on('load', (e) => {

    function show_message(type, title) {
        Swal.fire({
            position: 'center',
            icon: type,
            title: title,
            showConfirmButton: true,
        });
    }

    $('#register-form').validate({
        rules: {
            num_cta: {
                required: true,
                digits: true
            },
            pass: {
                required: true
            },
            nombre: {
                required: true
            },
            primer_apellido: {
                required: true
            },
            segundo_apellido: {
                required: true
            },
            genero: {
                required: true
            },
            fecha_nac: {
                required: true,
                date: true
            }
        },
        messages: {
            num_cta: {
                required: "Número de cuenta requerido",
                digits: 'Ingrese número de cuenta válido'
            },
            pass: {
                required: "Contraseña requerida"
            },
            nombre: {
                required: 'Nombre requerido'
            },
            primer_apellido: {
                required: 'Primer apellido requerido'
            },
            segundo_apellido: {
                required: 'Segundo apellido requerido'
            },
            genero: {
                required: 'Género requerido'
            },
            fecha_nac: {
                required: 'Fecha de nacimiento requerida',
            }

        },
        submitHandler: function (form) {
            Swal.fire({
                title: '¿Los datos son correctos?',
                text: "Asegurate de que los datos ingresados sean correctos",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, registrar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        data: $(form).serialize(),
                        success: function (response) {
                            if (response === 'false') {
                                show_message('warning', 'Número de cuenta previamente registrado');
                            } else {
                                show_message('success', 'Alumno registrado correctamente');
                                // Reinicia el formulario
                                $('#register-form').get(0).reset();
                            }
                        },
                        error: function () {
                            show_message('error', '¡Ocurrió un error! Intenta más tarde');
                        }
                    });
                }
            });
        }
    });
});