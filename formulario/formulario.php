<?php 
    session_start();
    // Valida que se tenga una sesión abierta
    if (!isset($_SESSION['num_cta'])) {
        header('Location: ./');
    }
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formularios</title>
    <link rel="stylesheet" href="./css/global.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <header>
        <?php
            if (!isset($_SESSION['num_cta'])) {
                require_once('./navigation_bar_default.php');
            } else {
                require_once('./navigation_bar_login.php');
            }
        ?>
    </header>
    <main>
        <h1 class="text-center">Registrar alumno</h1>
        <section>
            <article id="form-container" class="d-flex justify-content-center align-items-center flex-column">
                <form action="./add_user.php" id="register-form" method="post" class="w-25">
                    <div class="container d-flex justify-content-center align-items-center flex-column">
                        <label for="num_cta" class="mt-3">N&uacute;mero de cuenta</label>
                        <input type="text" name="num_cta" id="num_cta" class="form-control">
                        <label for="nombre" class="mt-3">Nombre</label>
                        <input type="text" name="nombre" id="nombre" class="form-control">
                        <label for="primer_apellido" class="mt-3">Primer apellido</label>
                        <input type="text" name="primer_apellido" id="primer_apellido" class="form-control">
                        <label for="segundo_apellido" class="mt-3">Segundo apellido</label>
                        <input type="text" name="segundo_apellido" id="segundo_apellido" class="form-control">
                        <div class="d-flex align-items-center  mt-3 justify-content-between w-100">
                            <label for="">G&eacute;nero</label>
                            <div class="d-flex flex-column">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="genero" id="genero_h" value="H"
                                        checked>
                                    <label class="form-check-label" for="genero_h">
                                        Hombre
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="genero" id="genero_m" value='M'>
                                    <label class="form-check-label" for="genero_m">
                                        Mujer
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="genero" id="genero_o" value="O">
                                    <label class="form-check-label" for="genero_o">
                                        Otro
                                    </label>
                                </div>
                            </div>
                        </div>
                        <label for="fecha_nac" class="mt-3">Fecha nacimiento</label>
                        <input type="date" class="form-control" name="fecha_nac" id="fecha_nac">
                        <label for="pass" class="mt-3">Contrase&ntilde;a</label>
                        <input class="form-control"  type="password" name="pass" id="pass" >
                        <button id="btn-submit" class="btn btn-primary my-3">Registrar alumno</button>
                    </div>
                </form>
            </article>
        </section>
    </main>
    <script src="./bower_components/jquery/dist/jquery.min.js"></script>
    <script src="./bower_components/jquery-validation/dist/jquery.validate.min.js"></script>r
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script src="./js/register.js"></script>
</body>

</html>