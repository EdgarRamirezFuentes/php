<?php
    session_start();
    require_once('./init_data.php');
    // Solo funciona con peticiones POST
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $num_cta = (int)  $_POST['num_cta'];
        $pass = $_POST['pass'];
        $usuarios = $_SESSION['usuarios'];
        // Checa que exista un usuario con el número de cuenta recibido
        if (isset($usuarios[$num_cta])) {
            // Obtiene los datos del usuario
            $info_usuario = $usuarios[$num_cta];
            // Valida que la contraeña sea válida
            if ($pass == $info_usuario['contrasenia']) {
                // Establece los valores de la sesión
                $datos = array(
                'num_cuenta' => $num_cta, 
                'nombre' => $info_usuario['nombre'],
                'primer_apellido' => $info_usuario['primer_apellido'],
                'segundo_apellido' => $info_usuario['segundo_apellido'],
                'fecha_nac' => $info_usuario['fecha_nac']
            );
                $_SESSION['num_cta'] = $datos ;
            } else {
                // Contraseña incorrecta
                echo 'false';
            }
        } else {
            // No existe el número de cuenta
            echo 'false';
        }
    } else {
        // Solo se puede acceder mediante petición POST
        header('Location: ./');
    }
?>