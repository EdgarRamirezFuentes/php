<?php 

//Realizar una expresión regular que detecte emails correctos.
$email_regex = '/^[a-z0-9][-a-z0-9._]+@([\-a-z0-9]+.) +[a-z]{2,5}$/i';


//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$curp_regex = '/^[a-z][aeiou][a-z]{2}[0-9](0[1-9]|1[0-2])([0-2][0-9]|3[0-1])[as|bs|cl|cs|df|gt|hg|mc|ms|nl|pl|qr|sl|tc|tl|yn|ne|bc|cc|cm|dg|gr|jc|mn|nt|oc|qt|sp|sr|ts|vz|zs][bcdfghjklmnpqrstvwxyz]{3}[0-9]{2}$/i'; 


//Realizar una expresion regular que detecte palabras de longitud mayor a 50 formadas solo por letras.
$palabras_regex = '/[a-z]{51,}(\s[a-z]){51,}*/i';


//Crea una funcion para escapar los simbolos especiales.

function escapar(&$cadena) {
    $cadena = preg_quote($cadena);
}

//Crear una expresion regular para detectar números decimales.
$decimales_regex = '^/[0-9]+\.[0-9]+$/';

?>