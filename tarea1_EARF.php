<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1 class="text-center">Tarea 1 - Ram&iacute;rez Fuentes Edgar Alejandro</h1>
    <main>
        <section class="text-center d-flex">
            <article class="col-6">
                <h2>Piramide</h2>
                <?php
                    $row = '*';
                    define('MAX_ROWS', 30);
                    // Using only echo
                    for ($i = 0; $i <= constant('MAX_ROWS'); $i++) {
                        echo $row;
                        echo "<br>";
                        $row .= "*";
                    }
                ?>
            </article>
            <article class="text-center col-6">
                <h2>Rombo</h2>
                <?php
                    $row = '*';
                    $inverted_pyramid = array();
                    for ($i = 0; $i < constant('MAX_ROWS'); $i++) {
                        // Printing HTML Tags
                        echo "<p class=\"m-0\">";
                        echo $row;
                        array_push($inverted_pyramid, $row);
                        $row .= "*";
                        echo "</p>";
                    }
                    // Inverted pyramid
                    for ($i = constant('MAX_ROWS') - 1; $i > -1; $i--) {
                        echo "<p class=\"m-0\">";
                        echo $inverted_pyramid[$i];
                        echo "</p>";
                    }
                ?>
            </article>
        </section>
    </main>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

